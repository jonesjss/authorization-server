package br.com.caelum.authorization.domain;

import java.util.Arrays;
import java.util.List;

public class User {
    private final String username;
    private final String password;
    private final List<Role> roles;

    public User(String username, String password, Role... roles) {
        this.username = username;
        this.password = password;
        this.roles = Arrays.asList(roles);
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String[] getRoles() {
        return roles.stream().map(Enum::name).toArray(String[]::new);
    }
}
