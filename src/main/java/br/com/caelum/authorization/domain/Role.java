package br.com.caelum.authorization.domain;

public enum Role {
    USER,
    ADMIN;
}
