package br.com.caelum.authorization.login;

import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class LoginService implements UserDetailsService {
    private final LoginRepository repository;
    private final UserToResourceOwner converter;

    public LoginService(LoginRepository repository, UserToResourceOwner converter) {
        this.repository = repository;
        this.converter = converter;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        return repository.findByUsername(username)
                .map(converter::convert)
                .orElseThrow(() -> new UsernameNotFoundException("Cannot find user"));
    }
}
