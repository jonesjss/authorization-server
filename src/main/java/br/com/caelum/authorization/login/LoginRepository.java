package br.com.caelum.authorization.login;

import br.com.caelum.authorization.domain.Role;
import br.com.caelum.authorization.domain.User;
import org.springframework.stereotype.Repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Repository
public class LoginRepository {

    private static final Map<String, User> DATABASE = new HashMap<>();
    private static final String DEFAULT_PASSWORD = "{bcrypt}$2a$10$fZoEeffXDLOsZOKZMgx3WObOMrn04qrZuaQMroQ88errRP5.QINbG";

    static {
        DATABASE.computeIfAbsent("fwfurtado", username -> new User(username, DEFAULT_PASSWORD, Role.USER));
    }

    public Optional<User> findByUsername(String username)  {
        return Optional.ofNullable(DATABASE.get(username));
    }

}
